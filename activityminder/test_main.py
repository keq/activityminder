from datetime import datetime, timezone, timedelta
import random
import string

from activityminder.main import Bee

import dateutil


# Some of these tests will fail if ActivityWatch isn't already running


def test_prod_check():
    whitelist = [
        "".join(random.choice(string.ascii_letters) for i in range(10))
        for i in range(30)
    ]

    bee = Bee("", "", whitelist)

    for term in bee.whitelist:
        assert bee.is_productive(term)


def test_date_range_check():
    checks = [(datetime.now(timezone.utc) - timedelta(seconds=x)) for x in range(60)]

    bee = Bee("", "", [])
    bee.get_ago = lambda: datetime.now(timezone.utc) - timedelta(seconds=100)

    # cause assignment of ago and now
    events = bee.get_nonafk_events()

    for check in checks:
        assert bee._is_recent(check, 0)

    bee = Bee("", "", [])
    bee.get_ago = lambda: datetime.now(timezone.utc) - timedelta(seconds=100)

    # test against last 2? events
    events = bee.get_nonafk_events()
    for event in events[:-3:-1]:
        assert bee._is_recent(dateutil.parser.parse(event["timestamp"]), 0)


def test_get_prod_seconds():
    bee = Bee("", "", [])
    bee.get_ago = lambda: datetime.now(timezone.utc) - timedelta(seconds=100)

    # allows any string
    bee.whitelist.append("")

    secs = bee.get_prod_secs()

    assert secs > 0

    # don't allow anything
    bee.whitelist = ["\0"]

    assert bee.get_prod_secs() == 0
